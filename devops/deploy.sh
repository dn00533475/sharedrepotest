#!/bin/bash

#set -x

commit_branch=$1
commit_value=$2
job_path=$3
release_name=$4
action=$5
#mkdir -p PkgLocation/Consolidated-Package
#PackageLocation="PkgLocation"

if [ $6 == 'DUMMY' ]
then
        if [ $commit_branch == 'release' ];
        then
                ENVIRONMENT="ctPOC";
        elif [ $commit_branch == 'develop' ];
        then
                ENVIRONMENT="POC";
        elif [ $commit_branch == 'hotfix_master' ];
        then
                ENVIRONMENT="preprodPOC";
        fi
else
        ENVIRONMENT=$6
fi

head_commit=$(git rev-parse HEAD)

####### Package Creation

if [ $commit_value != 'HEAD' ]
then
	if [ $release_name = 'DUMMY' ]
	then
		echo "Provide Release Name for the consolidated package being created"
		exit 1
	fi
fi
chmod 777 devops/sf_package_create.py

if [ $action == 'CREATE' ]
then
        #echo "create package"
        git diff --no-renames -w $commit_value^ --name-status | grep -v 'classes.*meta.xml' | devops/sf_package_create.py -o $job_path/$head_commit
fi


####### Package Validation

if [ $action != 'DEPLOY' ]
then
	if [[ $commit_value = 'HEAD' && $release_name = 'DUMMY' ]]
	then
		echo "BL Package - HEAD commit, already validated before BL Merge"
	fi

	if [ $release_name != DUMMY ]
	then
		if [ $action = 'CREATE' ]
		then
			echo "Validate Consolidated Package"
        	mkdir -p $job_path/testpackage/force-app/main/default/classes
        
			sh devops/pipeline_validation.sh $job_path $head_commit $ENVIRONMENT
		
			if [ `echo $?` -eq 0 ]
        		then
				#cp -pr $job_path/$head_commit $PackageLocation/Consolidated-Package/$release_name
            	#cp devops/sfdx-project.json $PackageLocation/Consolidated-Package/$release_name
				zip -r $release_name.zip $job_path/$head_commit
				
				curl -H "X-JFrog-Art-Api:AKCp8jQwmF5rynzgk3qcXsofcVWnM13fqT1KqTD18AG41UVdGLToXVMoWGZ8cL9pk5TXbbAuz" -XPUT https://artifactory.orange.be/artifactory/obe-sfvl-generic/${release_name}.zip -T ${release_name}.zip

            			#echo "$release_name,$commit_branch,$commit_value to $head_commit,$ENVIRONMENT,`date '+%d%b%y:%H%M%S'`" >> $PackageLocation/CP_Commit.txt
			else
				echo "Consolidated Package Validation Failed"
				exit 1
			fi
		else	
			echo "Validate Consolidated Package:"
			#cp -r $PackageLocation/Consolidated-Package/$release_name $job_path	
			curl -H "X-JFrog-Art-Api:AKCp8jQwmF5rynzgk3qcXsofcVWnM13fqT1KqTD18AG41UVdGLToXVMoWGZ8cL9pk5TXbbAuz" -XGET https://artifactory.orange.be/artifactory/obe-sfvl-generic/${release_name}.zip --output ${release_name}.zip
    
			pkg=`unzip -l ${release_name}.zip |grep "/" | head -1| awk -F"/" '{print $1}'`
			unzip ${release_name}.zip
			
			mkdir -p $job_path/testpackage/force-app/main/default/classes
			sh devops/pipeline_validation.sh $job_path $pkg $ENVIRONMENT

			if [ `echo $?` -eq 0 ]
			then
					echo "Consolidated Package $release_name Validation in $ENVIRONMENT is Successful"
			else
					echo "Consolidated Package $release_name Validation in $ENVIRONMENT has Failed"
					exit 1
			fi
			#	zip -r $release_name.zip $PackageLocation/Consolidated-Package/$release_name
			#	curl -v -u deployment_mm_customer_information_management:deployment_mm_customer_information_management -F "raw.directory=/mm_customer_information_management/" -F "raw.assetN=@$job_path/$release_name.zip" -F "raw.assetN.filename=$release_name.zip" "https://dot-portal.de.pri.o2.com/nexus/service/rest/v1/components?repository=raw-private"


		fi
	fi
fi	


####### Package Deployment


if [ $action != 'VALIDATE' ]
then
	if [[ $commit_value == 'HEAD' && $release_name == 'DUMMY' ]]
	then
		git checkout $commit_branch
		BL=`git log | grep -C5 $head_commit | grep 'Merge branch' |awk -F"'" '{print $2}'|sed '/^$/d'`
		#git branch -a|grep "/$BL$" | xargs 1 bash -c 'echo "$1 `git log --pretty=format:"%H %an" $1^..$1`"' _ > /tmp/BL_author
		#BL_author=`cat /tmp/BL_author | awk ' { for (i=3; i<=NF; i++) printf $i" " }'`
		if [ $BL == 'develop' ]
		then
			BL="BackMerge_`date '+%d%b%y:%H%M%S'`"
		fi

		sfdx force:source:deploy  --targetusername=$ENVIRONMENT --sourcepath=$job_path/$head_commit/force-app

		if [ `echo $?` -eq 0 ]
		then
				echo "$BL Package deployed to $ENVIRONMENT successfully"
		else
				echo "$BL Package deployment to $ENVIRONMENT has Failed"
				exit 1
		fi

		BLnew=`echo $BL|tr -s '/' '-'`
		#cp -pr $job_path/$head_commit $PackageLocation/BL-Package/$BLnew
		zip -r ${head_commit}.zip $job_path/$head_commit
		
		curl -H "X-JFrog-Art-Api:AKCp8jQwmF5rynzgk3qcXsofcVWnM13fqT1KqTD18AG41UVdGLToXVMoWGZ8cL9pk5TXbbAuz" -XPUT https://artifactory.orange.be/artifactory/obe-sfvl-generic/${head_commit}.zip -T ${head_commit}.zip
		#echo "$BL,$BL_author,$head_commit,$ENVIRONMENT,`date '+%d%b%y:%H%M%S'`" >> $PackageLocation/DeploymentHistory.csv
	fi

	if [[ $release_name != DUMMY && $action != 'CREATE' ]]
	then
		curl -H "X-JFrog-Art-Api:AKCp8jQwmF5rynzgk3qcXsofcVWnM13fqT1KqTD18AG41UVdGLToXVMoWGZ8cL9pk5TXbbAuz" -XGET https://artifactory.orange.be/artifactory/obe-sfvl-generic/${release_name}.zip --output ${release_name}.zip

		pkg=`unzip -l ${release_name}.zip |grep "/" | head -1| awk -F"/" '{print $1}'`
		unzip ${release_name}.zip
		sfdx force:source:deploy  --targetusername=$ENVIRONMENT --sourcepath=${pkg}/force-app
		
		if [ `echo $?` -eq 0 ]
		then
				echo "Consolidated Package $release_name deployed to $ENVIRONMENT successfully"
		else
				echo "Consolidated Package $release_name deployment to $ENVIRONMENT has Failed"
				exit 1
		fi
	
		#commit="`grep "^$release_name," $PackageLocation/CP_Commit.txt | awk -F',' '{print $3}'`"
		#echo "$release_name,$commit_branch,$commit,$ENVIRONMENT,`date '+%d%b%y:%H%M%S'`" >> $PackageLocation/DeploymentHistory.csv
	fi
fi	
