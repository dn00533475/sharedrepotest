#!/bin/bash

#set -x

if [ "$1" == 'Release-R7' ]; 
then  
	ENVIRONMENT="ctPOC";  
elif [ "$1" == 'develop' ]; 
then  
	ENVIRONMENT="POC"; 
elif [ "$1" == 'HotFix_master' ]; 
then  
	ENVIRONMENT="preprodPOC"; 
fi

head_commit=$(git rev-parse HEAD)

chmod 777 devops/sf_package_create.py

git diff --no-renames -w $(git merge-base origin/$2 origin/$1) origin/$2 --name-status | grep -v 'classes.*meta.xml' | devops/sf_package_create.py -o $3/$head_commit

mkdir -p $3/testpackage/force-app/main/default/classes

sh devops/pipeline_validation.sh $3 $head_commit $ENVIRONMENT

if [[ "`echo $?`" -ne "0" ]];then echo "Validation Job Failed"; exit 1; fi

if [ "$CI_MERGE_REQUEST_TITLE" = "Merge develop into release" ]
then
	URL_acceptMR="https://gitlab.orange.be/api/v4/projects/386/merge_requests/$CI_MERGE_REQUEST_IID/merge"
	URL_listMR="https://gitlab.orange.be/api/v4/projects/386/merge_requests?state=opened"
	request=`curl --request GET --header "PRIVATE-TOKEN: 7Ao9ToawayD3z4x1GTdM" --header "Content-Type: application/json" $URL_listMR| grep "Merge develop into release" | sed 's/^\[//g' | sed 's/\]$//g'`

	echo $request

	echo $CI_MERGE_REQUEST_IID

	curl --request PUT --header "PRIVATE-TOKEN: 7Ao9ToawayD3z4x1GTdM" --header "Content-Type: application/json" --data "$request" $URL_acceptMR

fi

echo ""
echo " Running the Sonar Scan now"
echo ""

sonar-scanner \
  -Dsonar.projectKey="crm_obe-sfvl-repo_AXouLpCB-0r58RqQ3VKu" \
  -Dsonar.sources=$3/$head_commit \
  -Dsonar.host.url=https://sonarqube.orange.be \
  -Dsonar.login=56572fc2692354e631c459f271c16dc64201d31d

echo ""
echo ""
echo "Verify the Sonar Scan URL mentioned against QUALITY GATE STATUS:"
echo ""