#!/usr/bin/bash

#set -x 

MAIN_PACKAGE=$1
DIFF_PACKAGE=$2

TEST_CLASSES=()
CLASS_PATH="${DIFF_PACKAGE}/force-app/main/default/classes"
if [ -d $CLASS_PATH ]; then
    for CLASS_FILE_PATCH in $CLASS_PATH/*
    do
        CLASS_NAME=$(echo "$CLASS_FILE_PATCH" | perl -lne 'print $1 if /.*\/(.*).cls/')
        if [[ $CLASS_FILE_PATCH != *"-meta.xml"* ]] && [[ $CLASS_FILE_PATCH == *".cls"* ]]; then
            if [[ $CLASS_FILE_PATCH =~ [T|t][E|e][S|s][T|t] ]] && [[ ! "${TEST_CLASSES[*]}" =~ $CLASS_NAME ]]; then
                TEST_CLASSES+=("$CLASS_NAME")
            else
                if [[  ! " ${TEST_CLASSES[*]}" =~ ${CLASS_NAME}.?[tseTSE]+.cls ]]; then
                    TESTCLASS=$(find $CLASS_PATH -type f -regex ".*${CLASS_NAME}.?[tseTSE]+.cls" )
                    if [[ $TESTCLASS != "" ]]; then
                        CLASS_FILE2=$(echo "$TESTCLASS" | perl -lne 'print $1 if /.*\/(.*).cls/')
                        TEST_CLASSES+=("$CLASS_FILE2")
                    else
                        echo "checking in mainpackage"
                        TESTCLASS2=$(find $MAIN_PACKAGE -type f -regex ".*${CLASS_NAME}.?[tseTSE]+.cls" )
                        if [[ $TESTCLASS2 != "" ]]; then
                            CLASS_FILE3=$(echo "$TESTCLASS2" | perl -lne 'print $1 if /.*\/(.*).cls/')
                            TEST_CLASSES+=("$CLASS_FILE3")
                        fi
                    fi
                fi
            fi
        fi
    done
fi

READY_FOR_CMD=$(printf ",%s" "${TEST_CLASSES[@]}" | cut -c 2-)
echo "$READY_FOR_CMD"


