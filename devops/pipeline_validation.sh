#!/bin/bash

#set -x
#$1 = $CI_PROJECT_DIR
#$2 = $head_commit

MAIN_PACKAGE="$1/force-app"
TestPackage="$1/testpackage/force-app/main/default/classes"
PACKAGESPATH="$1/$2"
environment=$3
 
for f in `ls $PACKAGESPATH/force-app/main/default/classes | awk -F'.' '{print $1}'|grep -iv test|sort|uniq`
do
	cp $MAIN_PACKAGE/main/default/classes/$f*[tT][eE][sS][tT]*cls* $TestPackage 2>/dev/null

	if [ $? -ne 0 ]; then
		echo ""
		echo "ISSUE: test class for $f not available"
	fi

	cp $PACKAGESPATH/force-app/main/default/classes/$f*cls* $TestPackage
done
 
if [ -d "$PACKAGESPATH/force-app/main/default" ]; then

	echo "Validating package against $environment environment"
	sfdx force:source:deploy  --targetusername=$environment --sourcepath=$PACKAGESPATH/force-app --checkonly
	if [ $? -eq 0 ]; then
	   echo "Package Validation against Target Org $environment is Successful"
	else
	   echo "Package Validation against Target Org $environment has failed, Failing the job"
	   exit 1
	fi  
  
	#test_classes="`sh devops/get_test_classes_from_package.sh $MAIN_PACKAGE $1/testpackage`"
	cd $TestPackage
	test_classes="`ls *[tT][eE][sS][tT]*cls* |grep -v 'meta.xml' | sed 's/\.cls//g'|tr -s ' ' ','`"
    cd -

	echo $test_classes

	echo ""
	echo " *** verifying code coverage ***"
	
        #sfdx force:source:deploy --targetusername=$environment --sourcepath=$1/testpackage/force-app --testlevel RunSpecifiedTests --runtests=$test_classes --checkonly


        sfdx force:source:deploy --targetusername=$environment --sourcepath=$PACKAGESPATH/force-app --testlevel RunSpecifiedTests --runtests=$test_classes --checkonly

	 if [ $? -eq 0 ]; then
	   echo "Code Coverage Testing has Passed Successfully"
	 else
	   echo "Code Coverage is NOT enough, Failing the job"
	   #exit 1
	fi  
fi
